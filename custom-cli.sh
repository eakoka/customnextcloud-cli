#!/bin/bash
#=============================================================================
#
# script de gestion nextcloud
#
#=============================================================================

#=============================================================================
#variables générale
scriptver="1.0.0"
releasecanal="git"
nominstance="votreinstance"
nxdata="/var/www/nextcloud/data"
domainecollabora="votre.domaine.fr"
distrib=$(lsb_release -d)
linuxver=$(uname -r)
#=============================================================================
#fonction d'aide
helpscript() {
	echo "voici la liste des arguments reconnus:"
	echo "--update-collabora ou -uc : pour mettre a jour collabora code"
	echo "--docker-imageprune ou -dip : pour nettoyer les ancienne images docker"
	echo "--clear-varlog ou -cvl : pour nettoyé les logs de var/log"
	echo "--clear-nextcloudlog ou -cnl : pour nettoyé les logs de l'instance nextcloud"
	echo "--restart ou -r : pour relancer les services nextcloud"
	echo "--help ou -h pour afficher cette aide"
	echo "--version ou -v : pour obtenir plus de detail sur la version du script"
	echo ""
}
#=============================================================================
#fonction de mise a jour du docker collabora
update-collabora() {
	ConteneurId=$(docker ps --filter ancestor=collabora/code --format "{{.ID}}")
	domcol=$(echo $domainecollabora | sed -e "s/\./\\\\\\\./g")
	domcol='domain='$domcol
	#restorearg="$1"
	#if [ "$restorearg" != "restore" ];
	#then
	echo "Mise a jour de l'image de collabora"
	docker pull collabora/code
	#else
	#	shift
	#	echo "mode restore activé"
	#	echo "seul la restoration de l'instance sera effectuer"
	#fi
	echo "Arret de l'instance Collabora"
	docker stop $ConteneurId
	echo "Suppression de l'instance collabora"
	docker rm $ConteneurId
	echo "Déploiement de la nouvelle instance de collabora"
	docker run -t -d -p 127.0.0.1:9980:9980 -e $domcol --restart always --cap-add MKNOD collabora/code
	echo "Mise a jour de collabora terminé"
	echo ""
	echo "Redemarrage de $nominstance"
	restart
}
#=============================================================================
docker-imageprune() {
	docker image prune
	echo "image(s) supprimée(s)"
}
#=============================================================================
#fonction de redemmarage des service nextcloud
restart() {
	/etc/init.d/php7.4-fpm restart
	/etc/init.d/nginx restart
	/etc/init.d/coturn restart # seulement si vous utiliser le service coturn pour la voip nextcloudtalk
}
#=============================================================================
#fonction de nettoyage des var-log
clear-varlog() {
	rm -rv /var/log/*.gz
	rm -rv /var/log/*/*.gz
}
#=============================================================================
#fonction de nettoyage des logs de l'instance nextcloud
clear-nextcloudlog() {
	echo '' > $nxdata/nextcloud.log
	echo "Logs de l'instance $nominstance nettoyé"
}
#=============================================================================
#fonction de visionnage des logs de l'instance wolfnet
view-nextcloudlog() {
	echo ""
	echo "Affichage des données en brute :"
	tail -n20 $nxdata/nextcloud.log
}
#=============================================================================
#fonctions affichage de la version du script
version() {
	echo "customnextcloud-cli version $scriptver"
}
details() {
	if [ "$argument" == "1" ];
	then
		version
	fi
	echo "Canal: $releasecanal"
	echo "Instance: $nominstance"
	echo "Distrib $distrib"
	echo "Linux Kernel version: $linuxver"
}
#=============================================================================
#menu des arguments
version #affichage nom du script
while [ "$1" != "" ]; do
case $1 in
	--update-collabora|-uc)
		shift
		update-collabora
		argument="1"
	;;
	--docker-imageprune|-dip)
		shift
		docker-imageprune
		argument="1"
	;;
	--clear-varlog|-cvl)
		shift
		clear-varlog
		argument="1"
	;;
	--clear-nextcloudlog|-cnl)
		shift
		clear-nextcloudlog
		argument="1"
	;;
	--view-nextcloudlog|-vnl)
		shift
		view-nextcloudlog
		argument="1"
	;;
	--restart|-r)
		shift
		restart
		argument="1"
	;;
	--version|-v)
		shift
		details
		argument="1"
	;;
	--help|-h)
		shift
		helpscript
		argument="1"
	;;
	*)
		printf "argument $1 inconnu\n"
		helpscript
		exit 1
	;;
	esac
done
#=============================================================================
#gestion de l'absence d'argument
if [ "$argument" != "1" ];
then
	printf "Oops\nIl semblerait que je n'ai pas d'argument\n"
	helpscript
	exit 1
fi
